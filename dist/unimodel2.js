NewEden {
  Regions: [
    {
    RegionID: integer,
    RegionName: String,
    Description: String,
    Constellations: [
      {
        constellation_id: integer,
        name: String,
        position: {
          x: integer,
          y: integer,
          z: integer
          },
        systems: [
          {
            constellation_id: integer,
            name: String,
            planets: [
              {
                asteroid_belts: [
                  40000003: {
                    name: "Tanoo I - Asteroid Belt 1",
                    position: {
                      x: 161967513600,
                      y: 21288837120,
                      z: -73505464320
                    },
                    system_id: 30000001
                  }
                ],
                moons: [
                  40000004
                ],
                planet_id: 40000002
              },
            ],
            "position": {
              "x": -88510792599980580,
              "y": 42369443966878880,
              "z": -44513525346479660
            },
            "security_class": "B",
            "security_status": 0.8583240509033203,
            "star_id": 40000001,
            "stargates": [
              50000056,
              50000057,
              50000058
            ],
            "stations": [
              60012526,
              60014437
            ],
            "system_id": 30000001
          }
        ]

      },
      {
        constellation_id: 20000001,
        name: "San Matar",
        position: {
          x: -94046559700991340,
          y: 49520153153798850,
          z: -42738731818401970
        },
        region_id: 10000001,
        systems: [
          30000001,
          30000002,
          30000003,
          30000004,
          30000005,
          30000006,
          30000007,
          30000008
        ]
      }
    ]
  },
    {
    RegionID: 10000001,
    RegionName: "Name",
    Description: "words"
    Constellations: [
      {
      "name": "San Matar",
      "position": {
        "x": -94046559700991340,
        "y": 49520153153798850,
        "z": -42738731818401970
        },
      "systems": {
         30000001 : {
           "name": "Tanoo",
           "planets": [
             {
               "asteroid_belts": [
                 40000003
               ],
               "moons": [
                 40000004
              ],
              "planet_id": 40000002
            },
            {
              "asteroid_belts": [
                40000006
              ],
              "planet_id": 40000005
            },
            {
              "planet_id": 40000007
            },
            {
              "asteroid_belts": [
                40000009
              ],
              "moons": [
                40000010
              ],
              "planet_id": 40000008
            },
            {
              "moons": [
                40000012,
                40000013,
                40000014,
                40000015,
                40000016
              ],
              "planet_id": 40000011
            },
            {
              "asteroid_belts": [
                40000018
              ],
              "planet_id": 40000017
            }
          ],
          "position": {
            "x": -88510792599980580,
            "y": 42369443966878880,
            "z": -44513525346479660
          },
          "security_class": "B",
          "security_status": 0.8583240509033203,
          "star_id": 40000001,
          "stargates": [
            50000056,
            50000057,
            50000058
          ],
          "stations": [
            60012526: {
              "max_dockable_ship_volume": 50000000,
              "name": "Tanoo V - Moon 1 - Ammatar Consulate Bureau",
              "office_rental_cost": 332436,
              "owner": 1000126,
              "position": {
                "x": -1106145239040,
                "y": -145460060160,
                "z": 182618726400
              },
              "race_id": 2,
              "reprocessing_efficiency": 0.5,
              "reprocessing_stations_take": 0.05,
              "services": [
                "bounty-missions",
                "courier-missions",
                "interbus",
                "reprocessing-plant",
                "market",
                "stock-exchange",
                "cloning",
                "repair-facilities",
                "fitting",
                "news",
                "insurance",
                "docking",
                "office-rental",
                "loyalty-point-store",
                "navy-offices",
                "security-offices"
              ],
              "station_id": 60012526,
              "system_id": 30000001,
              "type_id": 2502
            },
            60014437
          ],
          "system_id": 30000001
        }
         }
       }
      }
    ]
  }
  ]
}
