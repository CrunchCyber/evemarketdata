New Eden {
  Regions: [
    {
      region_id: Number,
      name: String,
      constellations : [
        {
          constellation_id: Number,
          name: String,
          systems: [
            {
              system_id: Number,
              name: String,
              stations: [
                {
                  station_id: Number,
                  name: String,
                  type_id: Number
                }
              ]
            }
          ]
        }
      ]
    }
  ]
}
