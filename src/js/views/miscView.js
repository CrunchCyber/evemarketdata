import {elements} from './base';

export const renderTypeName = type => {
  const markup = `
  <strong>${type.name}</strong>
  `
  elements.typeName.insertAdjacentHTML('beforeend', markup);

}
