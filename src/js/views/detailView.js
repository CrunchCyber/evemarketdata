import {elements} from './base';

export const renderDetailWindow = type => {

  const markup =  `
  <div class="row" id="detailWindow">
    <div class="row" id="detailTopBar">
      <div class="col-md-12">
        <span>Information</span>
        <span class="float-right"> X </span>
      </div>
    </div>
      <div class="row" id="detailImg">
        <div class="col-md-12">
          <img src="../img/Renders/${type.typeID}.png" alt="${type.name}" style="width: 64px; height: 64px;">
          <span class="align-top">${type.name}</span>
        </div>
      </div>
      <div class="row" id="detailDescription">
        <div class="col-md-12" id="tabNav">
          <ul class="" id="myTab" role="tablist">
            <li class="">
              <a class="" id="home-tab" data-toggle="tab" href="#description" role="tab" aria-controls="description" aria-selected="true">Description</a>
            </li>
            <li class="">
              <a class="" id="profile-tab" data-toggle="tab" href="#detail" role="tab" aria-controls="detail" aria-selected="false">Atttribute</a>
            </li>
            <li class="">
              <a class="" id="profile-tab" data-toggle="tab" href="#detail" role="tab" aria-controls="detail" aria-selected="false">Fitting</a>
            </li>
            <li class="">
              <a class="" id="profile-tab" data-toggle="tab" href="#detail" role="tab" aria-controls="detail" aria-selected="false">Prerequisite</a>
            </li>
            <li class="">
              <a class="" id="profile-tab" data-toggle="tab" href="#detail" role="tab" aria-controls="detail" aria-selected="false">Variation</a>
            </li>
            </ul>
            <div class="tab-content" id="myTabContent" style="overflow: hidden;">
              <div class="tab-pane fade show active" id="description" role="tabpanel" aria-labelledby="description-tab">
                ${type.description}
              </div>
              <div class="tab-pane fade" id="detail" role="tabpanel" aria-labelledby="detail-tab">
                ${type.volume}
              </div>
          </div>
        </div>
      </div>
  </div>`
  elements.typeWindow.insertAdjacentHTML('beforebegin', markup);

}
