import {elements, formatNumber} from './base';

export const renderCompare = history => {
  const markup = `
  <table class="table-dark text-right table-striped w-100" >
   <thead>
     <tr>
       <td></td>
       <td>avg. Price</td>
       <td>min. Price</td>
       <td>max. Price</td>
       <td>Order Count</td>
       <td>Volume</td>
     </tr>
   </thead>
   <tbody>
     <tr>
       <td>Yesterday</td>
       <td class="">${formatNumber(history.yesterday.average)}</td>
       <td class="">${formatNumber(history.yesterday.lowest)}</td>
       <td class="">${formatNumber(history.yesterday.highest)}</td>
       <td class="">${formatNumber(history.yesterday.order_count)}</td>
       <td class="">${formatNumber(history.yesterday.volume)}</td>
     </tr>
     <tr>
       <td>last Week</td>
       ${compareHistory(history.yesterday, history.week)}
     </tr>
     <tr>
       <td>last Month</td>
       ${compareHistory(history.yesterday, history.month)}
     </tr>
     <tr>
       <td>last 3 Months</td>
       ${compareHistory(history.yesterday, history.threemonths)}
     </tr>
     <tr>
       <td>last 6 Months</td>
         ${compareHistory(history.yesterday, history.halfyear)}
     </tr>
     <tr>
       <td>last Year</td>
         ${compareHistory(history.yesterday, history.year)}
     </tr>
   </tbody>
 </table>
  `
  elements.compareTable.insertAdjacentHTML('beforeend', markup);
}

const compareHistory = (base, compareTo) => {

  let classArr = [];
  let valueArr = [];
  let markup = '';
  delete base['date'];
  delete compareTo['date'];
  for(let el in base){
    if(base[el] > compareTo[el]){
      if (el === 'order_count' || el === 'volume') {
        classArr.push('bg-danger');
      } else {
        classArr.push('bg-success');
      }
      valueArr.push(formatNumber((compareTo[el]-base[el])));
    } else if (base[el] === compareTo[el]) {
      classArr.push('');
      valueArr.push(0)
    } else if (base[el] < compareTo[el]) {
      if (el === 'order_count' || el === 'volume') {
        classArr.push('bg-success');
      } else {
        classArr.push('bg-danger');
      }
      valueArr.push(formatNumber((compareTo[el]-base[el])));
    } else {
      throw "error";
    }
  }
  for (let i = 0; i < classArr.length; i++) {
    markup += `<td class="${classArr[i]}">${valueArr[i]}</td>`;
  }
  return markup;

}
