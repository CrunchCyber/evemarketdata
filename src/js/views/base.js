export const elements = {
  buyOrderTable: document.querySelector('#buyOrdersTable'),
  sellOrderTable: document.querySelector('#sellOrdersTable'),
  typeName: document.querySelector('#orderView'),
  typeWindow: document.querySelector('#mainWindow'),
  compareTable: document.querySelector('#insertTable'),
  searchLocation: document.querySelector('#searchLocation'),
  regionSelect: document.querySelector('#regionSelect'),
  constellationSelect: document.querySelector('#constellationSelect'),
  systemSelect: document.querySelector('#systemSelect'),
  stationSelect: document.querySelector('#stationSelect')
}

export const formatNumber = (num) =>{
   let numSplit, int, dec;
   //- if expense
   //do rounding
   //add notation

   num = num.toFixed(2);

   numSplit = num.split('.');
   int = numSplit[0];
   if(int.length > 3){
     int = int.substr(0, int.length - 3) + ',' + int.substr(int.length - 3, 3);
   }
   dec = numSplit[1];

   return int + '.' + dec;
 };

export const formatDate = date => {
  let d = new Date(date);
  d.dd = d.getDate();
  d.mm = d.getMonth() === 0? 1 : d.getMonth();
  d.dateString = `${d.dd}-${d.mm}-${d.getFullYear()} / ${d.getHours()}: ${d.getMinutes()}:${d.getSeconds()}`
  return d.dateString;
}
