import {elements, formatNumber, formatDate} from './base';


export const clearOrders = () => {
  elements.buyOrderTable.innerHTML = '';
  elements.sellOrderTable.innerHTML = '';
};

export const renderBuyOrders = orders => {
  orders.forEach(renderOrder);
}

export const renderSellOrders = orders => {
  orders.forEach(renderOrder);
}

const renderOrder = (order) => {
  const markup = `
  <tr>
    <td class="text-right">${formatDate(order.issued)}</td>
    <td class="text-right">${formatNumber(order.price)}</td>
    <td class="text-right">${formatNumber(order.volume_remain)}/${formatNumber(order.volume_total)}</td>

    <td class="text-right">${parseJumps(order.range)}</td>
    <td class="text-right">${order.duration}</td>
  </tr>
  `
  if(order.is_buy_order === true){
    elements.buyOrderTable.insertAdjacentHTML('beforeend', markup);
  } else {
    elements.sellOrderTable.insertAdjacentHTML('beforeend', markup)
  }
}

//<td class="text-right">${order.min_volume}</td>


const parseJumps = range => {
  if(parseInt(range)){
    range = range + ' jumps'
  }else{
    range = range.charAt(0).toUpperCase() + range.slice(1);
  }
  return range;
}
