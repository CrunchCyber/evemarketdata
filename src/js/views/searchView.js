import {elements} from './base';


// onchange="locationController(this.val)"
export const displayRegions = list => {
    const markup = getOptions(list);  
    elements.regionSelect.insertAdjacentHTML('beforeend', markup);
}

export const displayConstellations = list => {
    const markup = getOptions(list);  
    elements.constellationSelect.insertAdjacentHTML('beforeend', markup);
}

export const displaySystems = list => {   
    const markup = getOptions(list);  
    elements.systemSelect.insertAdjacentHTML('beforeend', markup);
}

export const displayStations = list => {
    const markup = getOptions(list);  
    elements.stationSelect.insertAdjacentHTML('beforeend', markup);
}

// export const clearSelect = item => {
//     item = 'select';
//     const clean = document.querySelectorAll("select"); 
//     clean.innerHTML = '';
// }

const getOptions = list =>{
    let options = [];
    for(let item in list){    
        let entry = `<option value="${list[item].ID}">${list[item].name}</option>`;        
        options.push(entry);
    }
    return options;
}


