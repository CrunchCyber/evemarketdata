import { queryType } from '../config';
import axios from 'axios';

export default class Type{
  
  constructor(id){
    this.id = id;
  }

  async getType(){
    try{
      const res = await axios(queryType[0] + this.id + queryType[1]);
      this.capacity = res.data.capacity;
      this.description = res.data.description;
      this.groupID = res.data.group_id;
      this.iconID = res.data.icon_id;
      this.mass = res.data.mass;
      this.name = res.data.name;
      this.packageVolume = res.data.package_volume;
      this.published = res.data.published;
      this.radius = res.data.radius;
      this.typeID = res.data.type_id;
      this.volume = res.data.volume;
    }catch(err){
      console.log(err);
    }
  }
}
