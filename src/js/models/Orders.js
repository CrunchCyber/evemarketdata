import { queryOrder } from '../config';
import axios from 'axios';

export default class Orders{
  constructor(regionID, typeID){
    this.regionID = regionID;
    this.typeID = typeID;
  }

  async getOrders(){
    let pageNum = 1;
    let hasEntry = true;
    this.orders = [];
    while(hasEntry === true){
      try {
        const res = await axios(queryOrder[0] + this.regionID + queryOrder[1] + pageNum + queryOrder[2] + this.typeID);
        if (typeof res.data !== 'undefined' && res.data.length > 0) {
        this.orders = res.data;
          pageNum++;
        }else{
          hasEntry = false;
        }
      } catch(error){
        console.log(error);
      }
    }
  }

  getOrderByStation(orders, stationID){
    this.stationOrders = [];
    for(let order of orders){
      if (order.location_id === parseInt(stationID)) {
        this.stationOrders.push(order);
      }
    }
    this.orders = this.stationOrders;
  }

  getOrderBySystem(orders, systemID){
    this.systemOrders = [];
    for(let order of orders){
      if (order.system_id === parseInt(systemID)) {
        this.systemOrders.push(order);
      }
    }
    this.orders = this.systemOrders;
  }

  getBuyOrders(orders){
    this.buyOrders = [];
    for(let order of orders){
      if (order.is_buy_order === true) {
        this.buyOrders.push(order);
      }
    }
  }

  getSellOrders(orders){
    this.sellOrders = [];
    for(let order of orders){
      if(order.is_buy_order === false){
        this.sellOrders.push(order);
      }
    }
  }

  sortOrders(orders, type){
    if (orders.length > 1) {
      let flag, done = false;
      while(done === false) {
        flag  = false;
        for (var i = 0; i < orders.length-1; i++) {
          if (type[1] === 'high'){
            if (orders[i][type[0]] < orders[i+1][type[0]]) {
              const temp = orders[i];
              orders.splice(i, 1);
              orders.splice(i+1, 0, temp);
              flag = true;
            }
          } else if (type[1] === 'low') {
            if (orders[i][type[0]] > orders[i+1][type[0]]) {
              const temp = orders[i];
              orders.splice(i, 1);
              orders.splice(i+1, 0, temp);
              flag = true;
            }
          } else {
              throw 'error';
          }
        }
        if (flag === false) {
          done = true;
        }
      }
    }
  }
}
