import { queryHistory } from '../config';
import axios from 'axios';

export default class History{
  constructor(regionID, typeID){
    this.regionID = regionID;
    this.typeID = typeID;
  }

  async getHistory(){
    this.history = [];
    try{
      const res = await axios.get(queryHistory[0] + this.regionID + queryHistory[1] + this.typeID)
      this.history = res.data;
    }catch (err){
      throw err;
    }
  }

  // getToday(today){
  //
  //   return {average: 'NA', volume: 'NA', order_count: today.length }
  //
  // }

  getYesterday(){
    const l = this.history.length;
    return this.history[l-1];
  }

  getWeekAgo(){
    const d = new Date();
    d.setDate(d.getDate()-7);
    const n = d.toISOString().substring(0, 10);
    for(let entry of this.history){
      if (entry.date === n) {
        return entry;
      }
    }
  }

  getMonthkAgo(){
    const d = new Date();
    d.setMonth(d.getMonth()-1);
    const n = d.toISOString().substring(0, 10);
    for(let entry of this.history){
      if (entry.date === n) {
        return entry;
      }
    }
  }

  getThreeMonthkAgo(){
    const d = new Date();
    d.setMonth(d.getMonth()-3);
    const n = d.toISOString().substring(0, 10);
    for(let entry of this.history){
      if (entry.date === n) {
        return entry;
      }
    }
  }

  getHalfYearAgo(){
    const d = new Date();
    d.setMonth(d.getMonth()-6);
    const n = d.toISOString().substring(0, 10);
    for(let entry of this.history){
      if (entry.date === n) {
        return entry;
      }
    }
  }

  getYearAgo(){
    const d = new Date();
    d.setMonth(d.getMonth()-12);
    const n = d.toISOString().substring(0, 10);
    for(let entry of this.history){
      if (entry.date === n) {
        return entry;
      }
    }
  }

  getAveragePrice(history, length){
    let avrgPrice = [];
    for(let price of history){
      avrgPrice.push(price.average);
    }

    return avrgPrice.splice(0,length);
  }

  getLabel(history, length){
    let date = [];
    for(let price of history){
    date.push(price.date);
    }
    let start = date.length - length;
    return date.splice(start,date.length);
  }

  getVolume(history, length){
    let volume = [];
    for(let price of history){
      let temp = price.volume;
      volume.push(temp);
    }

    return volume.splice(0,length);
  }

  getOrderCount(history, length){
    let orderCount = [];
    for(let price of history){
      orderCount.push(price.order_count);
    }

    return orderCount.splice(0,length);
  }

  getMinPrice(history, length){
    let min_price = [];
    for(let price of history){
      min_price.push(price.lowest);
    }

    return min_price.splice(0,length);
  }
  getMaxPrice(history, length){
    let max_price = [];
    for(let price of history){
      max_price.push(price.highest);
    }

    return max_price.splice(0,length);
  }



}
