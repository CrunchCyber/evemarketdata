import axios from 'axios';

export default class Location{
  constructor(){
  }

  async getRegionList(){
    this.regionList= [];
    try{
      const res = await axios('https://esi.evetech.net/latest/universe/regions/?datasource=tranquility');
      this.regionList = res.data;

    }catch (err){
      throw err;
    }
  }

  async getRegionDetails(list){
    let tempRegionList = [];  
    for(let id of list){
      let region = {};
      region.ID = id;
      try{
        const res = await axios('https://esi.evetech.net/latest/universe/regions/' + id + '/?datasource=tranquility');
        region.name = res.data.name;
        region.constellations = res.data.constellations;
      }catch (err){
        throw err;
      }
      tempRegionList.push(region);
    }
    this.regionList = tempRegionList;
  }

  async getConstellationList(list){
    let tempConstellationsList = [];
    for(let id of list){
      let constellation = {};
      constellation.ID = id;
      try{
        const res = await axios('https://esi.evetech.net/latest/universe/constellations/' + id + '/?datasource=tranquility&language=en-us');

        constellation.name = res.data.name;
        constellation.systems = res.data.systems;
      }catch (err){
        throw err;
      }
      tempConstellationsList.push(constellation);
    }
    this.constellationList = tempConstellationsList;  
   }

   async getSystemList(list){
    let tempSystemList = [];
    for(let id of list){
      let system = {};
      system.ID = id;
      try{
        const res = await axios('https://esi.evetech.net/latest/universe/systems/' + id + '/?datasource=tranquility&language=en-us');
        system.name = res.data.name;
        system.stations = res.data.stations;
      }catch (err){
        throw err;
      }
      tempSystemList.push(system);
    }
    this.systemList = tempSystemList;  
   }

   async getStationList(list){
    let tempStationList = [];
    for(let id of list){
      let station = {};
      station.ID = id;
      try{
        const res = await axios('https://esi.evetech.net/latest/universe/stations/' + id + '/?datasource=tranquility&language=en-us');
        station.name = res.data.name;
      }catch (err){
        throw err;
      }
      tempStationList.push(station);
    }
    this.stationList = tempStationList;  
   }
}
