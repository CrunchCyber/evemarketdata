import * as historyView from '../views/historyView';
import History from '../models/History';
import Chart from 'chart.js';

export const historyController = async (searchQuery,state) => {



  if(searchQuery.hasOwnProperty('regionID') && searchQuery.hasOwnProperty('typeID')){
    searchQuery.regionID = parseInt(searchQuery.regionID)
    state.history = new History(searchQuery.regionID, searchQuery.typeID);
  
    await state.history.getHistory();
  
    state.history.yesterday = state.history.getYesterday();
    state.history.week = state.history.getWeekAgo();
    state.history.month = state.history.getMonthkAgo();
    state.history.threemonths = state.history.getThreeMonthkAgo();
    state.history.halfyear = state.history.getHalfYearAgo();
    state.history.year = state.history.getYearAgo();
    historyView.renderCompare(state.history);
  
    let length = 30;
    const chartData = {
      labels: state.history.getLabel(state.history.history, length),
      datasets: [{
        type: 'line',
        label: "average Price",
        borderColor: 'rgb(230,194,41)',
        radius: 0,
        borderWidth: 2,
        fill: false,
        data: state.history.getAveragePrice(state.history.history, length),
        yAxisID: 'y-axis-1'
      },
      {
        type: 'line',
        borderColor: 'rgb(241,113,5)',
        radius: 0,
        borderWidth: 2,
        backgroundColor: 'rgba(241,113,5,0.2)',
        fill: '2',
        data: state.history.getMinPrice(state.history.history, length),
        label: 'min Price',
        yAxisID: 'y-axis-1',
      },
      {
        type: 'line',
        borderColor: 'rgb(241,113,5)',
        backgroundColor: 'rgba(241,113,5,0.2)',
        radius: 0,
        borderWidth: 2,
        fill: false,
        data: state.history.getMaxPrice(state.history.history, length),
        label: 'max Price',
        yAxisID: 'y-axis-1',
      },
      {
        type: 'line',
        label: "Order Count",
        borderColor: 'rgb(23,190,187)',
        radius: 0,
        borderWidth: 2,
        fill: false,
        data: state.history.getOrderCount(state.history.history, length),
        yAxisID: 'y-axis-3'
      },
      {
        type: 'bar',
        label: "Volume",
        borderWidth: 2,
        borderColor: 'rgb(12,104,107)',
        backgroundColor: 'rgba(12,104,107,0.5)',
        data: state.history.getVolume(state.history.history, length),
        yAxisID: 'y-axis-2'
      }
      ]
    }
  
    const ctx = document.querySelector('#historyChart').getContext('2d');
    let chart = new Chart(ctx, {
      type: 'bar',
      data: chartData,
      options: {
        scales: {
          yAxes: [{
            type: 'linear',
            display: true,
            position: 'left',
            id: 'y-axis-1'
          },
          {
            type: 'linear',
            display: true,
            position: 'right',
            id: 'y-axis-2',
            maxTicksLimit: 6
          },
          {
            type: 'linear',
            display: true,
            position: 'right',
            id: 'y-axis-3'
          }]
        },
        plugins: {
          filler: {
            propagate: true
          }
        }
      }
    });
  }
  }