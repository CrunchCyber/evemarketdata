import * as searchView from '../views/searchView';
import Location from '../models/Location';
let doOnce = 0

export const locationController = async (searchQuery, state) =>{

    document.querySelectorAll('option').forEach(el => {
        el.parentNode.removeChild(el)
      });
    console.log(searchQuery);
    
    let id = parseInt(searchQuery.regionID);
    console.log(id);
    
    state.location = new Location();
    await state.location.getRegionList();
    await state.location.getRegionDetails(state.location.regionList);
    searchView.displayRegions(state.location.regionList);
    if(searchQuery.hasOwnProperty('regionID')){
        if(doesExist(id, state.location.regionList)){
        // i selected a region10000002
        // add some logic here to select the propper region
            
        let searchID = findPosition(id, state.location.regionList);

        await state.location.getConstellationList(state.location.regionList[searchID].constellations);
        searchView.displayConstellations(state.location.constellationList);
        await getSystem(state.location.constellationList,state);
        searchView.displaySystems(state.location.systemList); 
        await getStation(state.location.systemList,state);
        searchView.displayStations(state.location.stationList);
        }else {
            console.log('does not exist');
        }
    }
           
}

const doesExist = (id, list) => {
    let result = false;
    for(let entry of list){
        if (entry.ID === id ) {
            result = true;
        }     
    }
    return result;    
}

const findPosition = (id, list) =>{
    let result = 0;
    for(let region of list){
        if (region.ID === id ) {
            break;
        } 
        result = result +1;
    }
    return result;
}

const getSystem = async (list,state) => {
    let tempList = [];
    for(let entry of list){
        let searchID = findPosition(entry.ID, list);        
        await state.location.getSystemList(state.location.constellationList[searchID].systems);
        for(let entry of state.location.systemList){
            tempList.push(entry);
        }     
    }  
    return state.location.systemList = tempList;      
}

const getStation = async (list,state) => {
    let tempList = [];
    for(let entry of list){     
        if(entry.stations !== undefined){
            let searchID = findPosition(entry.ID, list);        
            await state.location.getStationList(state.location.systemList[searchID].stations); 
            for(let entry of state.location.stationList){
                tempList.push(entry);
            }   
        }          
    }  
    return state.location.stationList = tempList;       
}

