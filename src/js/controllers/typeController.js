import * as miscView from '../views/miscView';
import Type from '../models/Type';

export const typeController = async (searchQuery,state) => {
    if(searchQuery.hasOwnProperty('typeID')){
        state.type = new Type(searchQuery.typeID);
        await state.type.getType();
        miscView.renderTypeName(state.type);
    }
    
}