import * as ordersView from '../views/ordersView';
import Orders from '../models/Orders';

export const ordersController = async (searchQuery, state) => { 
  if(searchQuery.hasOwnProperty('regionID') && searchQuery.hasOwnProperty('typeID')){
    try {        
      state.search = new Orders(searchQuery.regionID, searchQuery.typeID);
      await state.search.getOrders();
      if (searchQuery.hasOwnProperty('stationID')) {
        state.search.getOrderByStation(state.search.orders, searchQuery.stationID);
      } else if (searchQuery.hasOwnProperty('systemID')) {
        state.search.getOrderBySystem(state.search.orders, searchQuery.systemID);
      }
  
      state.search.getBuyOrders(state.search.orders);
      state.search.getSellOrders(state.search.orders);
  
      state.search.sortOrders(state.search.buyOrders, searchQuery.sort.buy);
      state.search.sortOrders(state.search.sellOrders, searchQuery.sort.sell);
  
      ordersView.renderBuyOrders(state.search.buyOrders);
      ordersView.renderSellOrders(state.search.sellOrders);
    } catch(err){
      console.log(err);
    }
  }
    
  }