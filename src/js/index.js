// Global app controller
import {elements} from './views/base';

import * as locationController from './controllers/locationController';
import * as ordersController from './controllers/ordersController';
import * as typeController from './controllers/typeController';
import * as historyController from './controllers/historyController';

const state = [];

let searchQuery = {
  // regionID:10000001,
  // stationID: 0,
  //  typeID: 34,
  sort: {
    buy: ['price', 'high'],
    sell: ['price', 'low']
  }
};

let urlString = window.location.href;
console.log(urlString);
let url = new URL(urlString);
console.log(url.searchParams.get('regionSelect'));
if(url.searchParams.get('regionSelect') !== null){
  searchQuery.regionID = url.searchParams.get('regionSelect');
}
if(url.searchParams.get('constellationSelect') !== null){
  searchQuery.constellationID = url.searchParams.get('constellationSelect');
}
if(url.searchParams.get('systemSelect') !== null){
  searchQuery.systemID = url.searchParams.get('systemSelect');
}
if(url.searchParams.get('stationSelect') !== null){
  searchQuery.stationID = url.searchParams.get('stationSelect');
}
if(url.searchParams.get('typeSelect') !== null){
  searchQuery.typeID = parseInt(url.searchParams.get('typeSelect'));
}




locationController.locationController(searchQuery, state);
ordersController.ordersController(searchQuery, state);
typeController.typeController(searchQuery,state);
historyController.historyController(searchQuery, state);

elements.searchLocation.addEventListener('change', e => {
  if (e.target.matches('#regionSelect')){
    searchQuery.regionID = e.target.value;
    locationController.locationController(searchQuery, state);

  }
  
})